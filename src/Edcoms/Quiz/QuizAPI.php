<?php

namespace Edcoms\Quiz;

class QuizAPI {

    private $app;
    private $apiURL;
    private $studentURL;
    private $joinURL;
    private $hostURL;
    private $returnURL;

    public function __construct() {
        $this->setapp("va-quiz-live");
        $this->setapiURL("http://local.quiz.ed/app_dev.php/api");
        $this->setstudentURL("http://local.pl.co.uk/user/quiz/join");
        $this->setjoinURL("http://localhost:3000/#/quiz");
        $this->sethostURL("http://localhost:3000/#/host");
        $this->setreturnURL("http://local.pl.co.uk/user/quiz/");
    }

    public function setapp($app) {
        $this->app = $app;
        return $this;
    }

    public function setapiURL($apiURL) {
        $this->apiURL = $apiURL;
        return $this;
    }

    public function getapiURL() {
        return $this->apiURL;
    }

    public function setstudentURL($studentURL) {
        $this->studentURL = $studentURL;
        return $this;
    }

    public function getstudentURL() {
        return $this->studentURL;
    }

    public function setjoinURL($joinURL) {
        $this->joinURL = $joinURL;
        return $this;
    }

    public function getjoinURL() {
        return $this->joinURL;
    }

    public function sethostURL($hostURL) {
        $this->hostURL = $hostURL;
        return $this;
    }

    public function gethostURL() {
        return $this->hostURL;
    }

    public function setreturnURL($returnURL) {
        $this->returnURL = $returnURL;
        return $this;
    }

    public function getreturnURL() {
        return $this->returnURL;
    }
    public function launchAction($teamId) {
       // $teamId = 21; //get id from POST
        $mode = "multiplayer"; // get mode from POST
        $message = '';
        $teamsRepo = $this->getDoctrine()->getManager('edcoms_cms')->getRepository('AppBundle:Team');
        $team = $teamsRepo->findOneBy(['id' => $teamId]);
        $teamName = $team->getName();
        $numberOfStudents = count($team->getMembers());
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $username = $user->getUsername();
        $userid = $user->getId();
        $url = $this->hostURL;
        /** First we create the teacher user or retrieve an existing teacher user */
        $response = $this->createUserAction($username, $userid, $teamName, $teamId, $numberOfStudents, 'teacher');
        /** if teacher exists, continue creating the game */
        if ($response->status === true && property_exists($response, 'user')) {

            $gameResponse = $this->createGameAction($username, $teamId, $mode, $numberOfStudents);
            if ($gameResponse->status === true && property_exists($gameResponse, 'game')) {

                $tokenResponse = $this->generateAccessTokenAction($username, $gameResponse->game->token);

                if ($tokenResponse->status === true) {
                    $url .= "/" . $username . "/" . $tokenResponse->token;
                } else {
                    $message = implode(', ', $tokenResponse->message);
                }
            } else {
                $message = implode(', ', $gameResponse->message);
            }
        } else {
            $message = implode(', ', $response->message);
        }
        return $this->redirect($url);
    }

    public function joinAction($teamId,$gameToken) {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $username = $user->getUsername();
        $userid = $user->getId();
        $url = '';
        $message = '';
        if ($teamId) {
                $response = $this->createUserAction($username, $userid, null, $teamId, 0, 'student');
                /** is user exists, join game */
                if ($response->status === true && property_exists($response, 'user')) {
                    $tokenResponse = $this->generateAccessTokenAction($username, $gameToken);
                    if ($tokenResponse->status === true) {
                        $url =$this->joinURL."/" . $username . "/" . $tokenResponse->token;
                    } else {
                        $message = implode(', ', $tokenResponse->message);
                    }
                } else {
                    $message = implode(', ', $response->message);
                }
        } else {
            $message = "Team not found.";
        }
        return $this->redirect($url);
    }

    private function createUserAction($username, $externalId, $teamName = null, $teamExternalId = null, $numberOfStudents = 0, $type) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'external-id' => $externalId,
            'type' => $type,
            'number-of-students' => $numberOfStudents,
        );
        if ($teamName !== null) {
            $params['team-name'] = $teamName;
        }

        if ($teamExternalId !== null) {
            $params['team-external-id'] = $teamExternalId;
        }
        $response = $this->makeCall("create-user", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function createGameAction($username, $teamExternalId, $mode, $numberOfStudents = null, $studentUrl = null) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'team-external-id' => $teamExternalId,
            'quiz' => 'Values Awards Quiz',
            'mode' => $mode,
            'return-url' => $this->returnURL,
        );
        if ($mode == 'classroom') {
            $params['number-of-students'] = $numberOfStudents;
        } elseif ($mode == 'multiplayer') {
            $params['student-url'] = $studentUrl;
            $params['number-of-students'] = $numberOfStudents;
        }
        $response = $this->makeCall("create-game", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function generateAccessTokenAction($username, $gameToken) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'token' => $gameToken,
        );
        $response = $this->makeCall("generate-access-token", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function generateReport() {
        $params = array(
            'app' => $this->app
        );
        $response = $this->makeCall("generate-report", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function createQuizAction($username,$teamExternalId) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'team-external-id' => $teamExternalId,
            'quiz' => 'Values Awards Quiz',
            'return-url' => $this->returnURL,
        );
        $response = $this->makeCall("create-quiz", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function createQuestionAction($username,$teamExternalId) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'team-external-id' => $teamExternalId,
            'quiz' => 'Values Awards Quiz',
            'return-url' => $this->returnURL,
        );
        $response = $this->makeCall("create-quiz", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function createAnswerAction($username,$teamExternalId) {
        $params = array(
            'app' => $this->app,
            'username' => $username,
            'team-external-id' => $teamExternalId,
            'quiz' => 'Values Awards Quiz',
            'return-url' => $this->returnURL,
        );
        $response = $this->makeCall("create-quiz", $params);
        if (empty($response->errorNo)) {
            return json_decode($response->response);
        }
    }

    private function makeCall($endpoint, $fields = []) {
        $connection = curl_init();
        curl_setopt_array($connection, [
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->apiURL . "/" . $endpoint,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_TIMEOUT => 30,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $fields,
        ]);
        $callResponse = new \stdClass();
        $callResponse->response = curl_exec($connection);
        $callResponse->errorNo = curl_errno($connection);
        $callResponse->error = curl_error($connection);
        $callResponse->info = curl_getinfo($connection);
        return $callResponse;
    }

}
